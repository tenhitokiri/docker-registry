# Configuración de Docker Registry con UI

## Comandos para subir

Ejecutar `docker images` para saber el id de la imagen

Con el Id de la imagen ejecutar `docker tag id direccion_ip:puerto/nombre_imaven:version`

Finalmente ejecutar `docker push direccion_ip:puerto/nombre_imaven:version`

## Solucionar error "HTTP response to HTTPS client"

Crear o modificar archivo en el CLIENTE /etc/docker/daemon.json

        { "insecure-registries":["myregistry.example.com:5000"] }

Reiniciar el Demonio de Docker

    sudo service docker restart
